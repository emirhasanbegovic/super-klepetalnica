// V tej datoteki je opredeljen objekt Klepet, 
// ki nam olajša obvladovanje funkcionalnosti uporabnika


var Klepet = function(socket) {
  this.socket = socket;
};


/**
 * Pošiljanje sporočila od uporabnika na strežnik z uporabo 
 * WebSocket tehnologije po socketu 'sporocilo'
 * 
 * @param kanal ime kanala, na katerega želimo poslati sporočilo
 * @param besedilo sporočilo, ki ga želimo posredovati
 */
Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};


/**
 * Uporabnik želi spremeniti kanal, kjer se zahteva posreduje na strežnik 
 * z uporabo WebSocket tehnologije po socketu 'pridruzitevZahteva'
 * 
 * @param kanal ime kanala, kateremu se želimo pridružiti
 */
Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};


function mkPic(message){
  var words = message.split(" ");
  var links = [];
  var i = 0, c = 0, n = '"', url;
  
  while(i<words.length){
    var ending = words[i].substring(words[i].length-4, words[i].length);
    if(words[i].indexOf("http")==0){
      if(ending == ".png" | ending == ".jpg" | ending == ".gif"){
        url = words[i];
        links[c] = url;
        c++;
      }
    }
    i++;
  }
  
  for(var i = 0; i < links.length; i++){
    links[i] = "<br><img src = "+n+links[i]+n+"style = width:200px;height:200px hspace = "+n+"20px;"+n+"></br>";
    words[words.length]=links[i];
  }
  return words.join(" ");
}

/**
 * Procesiranje ukaza, ki ga uporabnik vnese v vnosno polje, kjer je potrebno
 * najprej izluščiti za kateri ukaz gre, nato pa prebrati še parametre ukaza
 * in zahtevati izvajanje ustrezne funkcionalnosti
 * 
 * @param ukaz besedilo, ki ga uporabnik vnese v vnosni obrazec na spletu
 */
 
var a1 = [];
var a2 = [];
var made = false;

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  // Izlušči ukaz iz vnosnega besedila
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      // Sproži spremembo kanala
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      // Zahtevaj spremembo vzdevka
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      if (parametri) {

        var makeHTML_pic = mkPic(parametri[3]);
        
        
        for(var i in a1){
          console.log("rez: "+a1[i] +"   "+a2[i]);
        }
        
        var i = 0;
        for(; i < a2.length; i++){
          if(a2[i]==parametri[1]){
            this.socket.emit('sporocilo', {vzdevek: a1[i], besedilo: makeHTML_pic});
            sporocilo = '(zasebno za ' + a1[i]+"("+parametri[1] + ")" + makeHTML_pic;
            made=true;
            break;
          }
        }
        if(!made){
          this.socket.emit('sporocilo', {vzdevek: parametri[1], besedilo: makeHTML_pic});
          sporocilo = '(zasebno za ' + parametri[1] + "): "+ makeHTML_pic;
            
        }
      } else {
        sporocilo = 'Neznan ukaz';
      }
      break;
    case 'preimenuj': 
      besede.shift();
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      if (parametri) {
        a1.push(parametri[1]);
        a2.push(parametri[3]);
        for(var i in a1){
          console.log("rez: "+a1[i] +"   "+a2[i]);
        }
        sporocilo = 'preimenuj ' + parametri[1] +" "+ parametri[3];
      } else {
        sporocilo = 'Neznan ukaz';
      }
      case 'barva':
        document.getElementById("sporocila").style.backgroundColor=besede[1];
        document.getElementById("kanal").style.backgroundColor=besede[1];
      break;
    default:
      // Vrni napako, če pride do neznanega ukaza
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};